console. log("hello world");

//Statements, Syntax and Comments

//JS Statements usually end with semi-colon(;). are not required but we will use it to help us train to locate where a statement ends
//
//console.log - connects or printouts to the console of the web browser

//Note: convention: Breaking up long statements into multiple lines is common practice.

/*
Multiple Comments
*/

//Variables (memory) - is used to contain or store a data ; for 1:1 use

//Declare Variables:
//Syntax : let/const variablesName; var

// let/const/var is a keyword that is usually used in declaring a variable.

// let - can declare Variable even it has no data
// const - cannot declare Variable with no data
// = - assigment operator


const myVariable = "Hello Again";
console.log(myVariable);
//myVariable - considered as a string
// "" - specifies as a string


/*
Guides in writing the variables:
1. we can use 'let/const' keyword followed by the variable name of your choosing and use the assignment operator (=) to assign a value
2. Variable name sshould start in a lowercase character, use camelCase for multiple words

*/

//Best Practice in naming variables:
//1. When naming variables, it is important to create variables that are descriptive and indicative of the date it contains

let firstName = "Willie"; // good variable name
let lastName = "Doe";
let pokemon = 25000 // bad variable name

//2. it is better to start with lowercase letter. Bec. there are several keywords in JS that start in capital letter.

//3. Do not add spaces to your variable names. Use camelCase for multiple words or underscore
		// let first name = "Mike";
		let product_description = "sample";

//Declaring and initializing variables
//Initializing variables - the instance when a variable is given its initial/starting value
//Syntax: let/const variableName = value;
let productName = "desktop computer";
console.log(productName)

//if number - can be as is and without 2x or 1x quotation 
let productPrice = 18999;
console.log(productPrice)

const interest = 3.539;

//Reassigning variable values
productName = "Laptop";
console.log(productName);

//let variable can be reassigned with another value (reads what is current, from top to bottom)
//let variable cannot be re-declared within its scope

let friend = "Kate";
let anotherFriend = "John";
console.log(friend);

//syntax ; declare first the variable before the console.log
//const cannot be updated or re-declared
//values of constant cannot be changed and will simply return an error.
const pi = 3.14;
console.log(pi);

let anotherVariable; 

//Reassigning variables vs. initializing variables
//let - we can declare a variable first

let supplier;
//initialization is done after the variable has been declared
supplier = "John Smith Tradings";
console.log(supplier);

//reassign its initial value
supplier = "Zuitt Store";
console.log(supplier);

//const variables are variables with constant data. Therefore we should not re-declare, re-assign or even declare a const variabl without initialization.

//var vs. let/const

//var - is also used in declaring a variable. var is an ECMA script (ES1) feature ES1 (JS 1997).
//let/const was introduce as a new feature in ES6(2015)

//what makes let/const different from var?
//there are issues when we uused var in declaring variables. one of these is hoisting
//Hoisting - a JS default behaviour of moving declaration to the top.

a = 5;
console.log(a);
var a;

//Scope - esssentially means where these variables are available to use

// let/const can be used as a local/global scope 
// A block is a chunk of code bounded by {}. A block lives in curly braces.
//So a variable declared in a block with let/const is only available for use within that block

//global scope = can be accessed (console.log) anywhere 
const outerVariable = "Hello";


{
	// block/local scope = {}
	// can only be accessed (consoled) within the block/local scope
	const innerVariable = "hello again";
	console.log(innerVariable)
}

//console.log(innerVariable); // is not defined

//Multiple Varaible Declarations

let productCode = "DC017";
const productBrand = 'Dell';
console.log(productCode, productBrand);

//const let = "Hello";


//DATA TYPES

//STRINGS
//String are a series of characters that create a word, a phrase, a sentence or antyhing related to creating text.
//Strings in Javascript can be written using either a single (') or double (") quotation.

let country = 'Philippines';
let province = "Metro Manila";
console.log(typeof country);
console.log(typeof province);

//Concatenating Strings
//Multiple string values can be combined to create a single string using the "+" symbol
let fullAddress = province + ', ' + country;
console.log(fullAddress);

let greet = "I live in the " + country;
console.log(greet);

// Template literals (ES6) is the updated version of concatenation
// backticks 'dvdv'
// expression ${} 
console.log(`I live in the ${province}, ${country}`);

//Escape Character (\) in strings in combination with other characters can produce different effects 
// "\n" refers to creating a new line in between text

let mailAddress = 'Metro Manila\n\nPhilippines';
console.log(mailAddress);

let message = 'John\'s employees went home early';
console.log(message);

message = "John's employees went home early";
console.log(message);

console.log(

	`Metro Manila

	Hello

	Hello

	Philippines ${province}`)

//NUMBERS
//Integers/Whole Numbers
let headcount= 23;
console.log(typeof headcount);

//Decimal Nembers/Fractions
let grade = 98.7;
console.log(typeof grade);

//Exponential Notation
let planetDisctance = 2e10;
console.log(typeof planetDisctance);

//combining variable with number data type and string
console.log("John's grade last quarter is " + grade);
console.log(`John's grade last quarter is ${grade}`)

//BOOLEAN
//Boolean values are normally used to store values relating to the state of certain things.
//This will be useful in further discussions about creatiing loogic to make our application respond to certain scenarios

let isMarried = false;
let inGoodConduct = true;
console.log(typeof isMarried);
console.log(typeof inGoodConduct);

//OBJECTS

//Arrays
	//are a special king of data that is used to store multiple values. it can store different data types but is normally used to store similar data types

//similar data types
//syntax: let//const arrayName = [elementA, elementB, etc.]

let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades);
console.log(typeof grades);

//different data types
//storing diff data types inside an array is not recommended because it will not make sense to join the context of programming.
let details = ["John",, "Smith", 32, true]
console.log(details);

console.log(grades[1], grades [0]);

//Reassigning array elements

//let anime = ['one piece', 'one punch man', 'attack on titan']
//console.log(anime);
//anime[0] = 'kimetsu no yaiba'
//console.log(anime);


//using const
const anime = ['one piece', 'one punch man', 'attack on titan']
console.log(anime);
anime[0] = 'kimetsu no yaiba'
console.log(anime);

// the keyword const is a little misleading. It does not define a constant value. It defines a constant reference to a value.
/*

Because of this you can not:
	
	Reassign a constant value, constant array, constant object

BUT YOU CAN:

	Change an elements of constant array
	Change the properties of constant object

*/

//OBJECTS
//syntax
/*
	let/const objectName = {property: value, propertyB: value}

	gives an individual piece of information and it is called a property of the object.

	They're used to cereate complex data that contains pieces of information that are relevant to each other.
*/

let objectGrades = {
	firstQuarter: 98.7, 
	secondQuarter: 92.1,
	thirdQuarter: 90.2,
	fourthQuarter: 94.6
};

let person = {
	fullName: 'Juan Dela Cruz',
	age: 35,
	isMarried: false,
	contact: ['091234557890', '8123 4567'],
	address: {
		houseNumber: '345',
		city: 'Manila'
	}

		
}

console.log(person)

//dot notation
console.log(`${person.fullName} ${person.isMarried}`);
console.log(person.age);

console.log(person.contact[0]);

console.log(person.address.city)

//Miniactivty

firstName = "Willie";
lastName = "Pasajol";
console.log(firstName,lastName);

let firstWords = "I am a student of";
let lastWords = "Zuitt.";
console.log(firstWords + ' ' + lastWords);
console.log(`I am a student of ${lastWords}`);

let foodWars = ['ube halaya', 'bulalo', 'leche flan', 'french fries']
console.log(foodWars);

let me = {
	fullName: 'Willie',
	lastName: "Pasajol",
	isDeveloper: true,
	hasPortfolio: true,
	age: 30
	}
console.log(me)

//end of Miniactivity


//NUMBER

let numb1 = 5;
let numb2 = 6;
let numb3 = 5.5;
let numb4 = .5;

let numbString1 = "5";
let numbString2 = "6";

console.log(numbString1 + numbString2)//"56" strings were concatenated.
console.log(numb1 + numb2) //

//TYPE COERCION/FORCE COERCION
// is the automatic or implicit conversion of values from one data type to another
console.log(numbString1 + numb1) //"55" resulted in concatenation (string)
//Adding/Concatenating a string and a number will result into a string

//Mathematical Operations (+, -, *, /, %(modulo - gets the remainder))

//Modulo
console.log(numb1 % numb2); // (5/6) = remainder 5
console.log(numb2 % numb1); // (6/5) = remainder 1

console.log(numb2 - numb1);

let product = numb1 * numb2;
console.log(product);

console.log(numb3 % numb2);

//NULL
//It is used to intentionally express the absence of a value in a variable declaration/initialization
let girlfriend = null;
console.log(girlfriend);

let myNumber = 0;
let myString = '';
console.log(myNumber);
console.log(myString);
//using null compared to a 0 value and an empty string is much better for readability purposes.


//UNDEFINED
//Represents the state of a variable that has been declared but without an assigned value.
let sampleVariable;
console.log(sampleVariable); //undefined

//One clear difference between undefined and null is that for undefined, a variable was created but was not provided a value.
//null means that a variable was created and was assigned a value that does not hold any value/amount


//FUNCTION
//in JS, it is a line/s/block of codes that tell our application to perform a certain task when called/involved.

//FUNCTION DECLARATION
	//Defines a function with the function and specified parameters

/*

Syntax:

	function functionName() {
		code block / statement. the block of code will be executed once the function has been run/called/invoked.
	}

	function keyword => defined a js function
	function name => camelCase to name our function
	function block/statement => the statement which comprise the body of the function. This is where the code to be executed.
*/

	//function printName() {
	//	console.log("My name is John");
	//}

//Function Expression
	//a function expression can be stored in a variable

	//Anonymous function - a function without a name.
	let variableFunction = function() {
		console.log("Hello Again")
	}

	let funcExpression = function func_name() {
		console.log("Hello")
	}


//Function Invocation
	//The code inside a function is executed when the function is called/invoked.

//let's invoke/call the function that we declared

printName();

function declaredFunction() {
	console.log("Hello World")
} 

declaredFunction();

//How about the function expression?
//They are always invoked/called using the variable name.
variableFunction();

//Function scoping

//JS Scope
//Global and local Scope

//
let faveCharacter = "Nezuko-chan"; //global scope

function myFunction() {
	let nickName = "Jane"; //function scope
	console.log(nickName);
	
}	

//console.log(faveCharacter);
myFunction();
//console.log(nickName);// not defined

//Variables defined inside a function are not accessible (visible) from outside the function
//var/let/const are quite similar when declared inside a function
//Variables declared globally (outside of the function) have a global scope or it can access anywhere.

function showSum() {
	console.log(6 + 6);
}

showSum();

//PARAMETERS AND ARGUMENTS
//"name" is called a parameter
// parameter => acts as a named variable/container that exists ONLY inside of the function. Usually parameter is the placeholder of an actual value
let name = "Jane Smith";

function printName(name) {
		console.log(`My name is ${name}`);
	}

//argument is the actual value/data that we passed to our function

printName(name);

//Multiple Parameters and Arguments

function displayFullName(fName, lName, age) {
	console.log(`${fName} ${lName} is ${age}`)
}

displayFullName("Judy", "Medalla", 25);
//Provide less arguments than the expected parameters will automatically assign an undefined value to the parameter.
//Providing more arguments will not affect our function.


//Return keyword
	// The "return" statement allows the output of a function to be passed to the block of code that invoked the function
	//any line/block of code that comes after the return statement is ignored because it ends the function execution

	//return keyword is used so that a function may return a value.
	//after returning the value, the next statement will stop its process


	function createFullName(firstName, middleName, lastName) {
			return `${firstName} ${middleName} ${lastName}`
			console.log("I will no longer run because the function's value/result has not returned")
			}

	//result of this function(return) can be saved into a variable

		let fullName1 = createFullName("Tom", "Cruise", "Mapother");
		console.log(fullName1)

	//the result of a function without a return keyword will  not save the result in a variable;

	let fullName2 = displayFullName("William", "Bradley", "Pitt");
	console.log(fullName2)

	let fullName3 = createFullName("Jeffrey", "John", "Smith");
	console.log(fullName3);

//Miniactivity 03.04.22


function div1(no1, no2) {
			return (no1 / no2)
			console.log("Operation halted, no value returned.")
			}

let quotient = div1(100, 10);
	console.log(quotient);

console.log(`The result of the division is : ${quotient}`)


// let number1_input = prompt("Please enter your first number:", "");
// let number2_input = prompt("Please enter your second number:", "");

//Function as an argument

function argumentFunction() {
	console.log("This function was passed as an argument before the message was printed")
}



