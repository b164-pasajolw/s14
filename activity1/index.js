console.log("Hello World")

/*
let info1 = "First Name: "
let fName = "Willie"
console.log(info1 + fName)

let info2 = "Last Name: "
let lName = "Pasajol"
console.log(info2 + lName)
*/

console.log("FUNCTION \n\n")
function printUserInfo(firstName, lastName, age, hobbies, workAddress) {
	console.log(`First Name: ${firstName}`);
	console.log(`Last Name: ${lastName}`);
	console.log(`Age: ${age}`);
	console.log("Hobbies:")
	console.log(hobbies);
	console.log("Work Address:");
	console.log(workAddress);
	console.log(`${firstName} ${lastName} is ${age} years of age.`)
	console.log("This was printed inside the function");
	console.log(hobbies);
	console.log("This was printed inside the function");
	console.log(workAddress)
	}


let hobbies = ['Walking', 'Web Browsing', 'Hiking']
let workAddress = {
		houseNumber: "358",
		street: "Purok 3",
		city: "Lipa",
		province: "Batangas"
	}

printUserInfo("Willie", "Pasajol", 30, (hobbies), (workAddress));


console.log("RETURN FUNCTION \n\n")

	function returnFunction(hobbies1, hobbies2, hobbies3) {
		return `${hobbies1} ${hobbies2} ${hobbies3}`
		console.log("Operation halted, no data returned.")}

	let myHobbies = returnFunction("Walking,", "Hiking,", "and Web Browsing");
	console.log(`This was printed inside the function \n\n: ${myHobbies}`);


	function returnVAr(statement) {
		return (`${statement}`)}

	let isMarriedOne = returnVAr(true)
		console.log(`The value of isMarried is: ${isMarriedOne}`)

console.log("\n\n STRETCH GOAL");
	
	function addFunction() {
	console.log(`The sum of the numbers is: ${6 + 4}`)}

	function callFunction(addFunction) {
	addFunction();}

	callFunction(addFunction)


	function minusFunction() {
	console.log(`The difference of the numbers is: ${6 - 4}`)}

	function textFunction(minusFunction) {
	minusFunction();}

	textFunction(minusFunction)



	function multip(x1, x2) {
				return (x1 * x2)
				console.log("Operation halted, no value returned.")}

	let xFactor = multip(5, 2);
	console.log(`The product is : ${xFactor}`)


